package com.example.demospringtesting;

import org.springframework.stereotype.Service;

@Service
class GreetingService {
    public String greet() {
        return "Hello, World!";
    }
}