# Spring Boot Testing Guideline

## Tạo Project

Vào trang https://start.spring.io/ để tạo project.

Đảm bảo bạn đã có dependency trong pom.xml hay build.gradle 

pom.xml

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>
```

hoặc nếu bạn sử dụng Gradle:

build.gradle

```groovy
testCompile("org.springframework.boot:spring-boot-starter-test")
```

## Viết test case 

Test Application Context

Sử dụng annotation `@SpringBootTest` để load toàn bộ context (ApplicationContext). Viết dụ sau sử dụng mọi test rỗng với mục đích chỉ để test toàn bộ context của App được load thành công. 

```Java
@SpringBootTest
class DemoSpringTestingApplicationTests {

	@Test
	void contextLoads() {
	}

}
```

Nếu bạn muốn test hành vi của application, bạn có thể start app và listen connections giống như khi chạy production, sau đó gửi một HTTP request and kiểm tra response trả về.

```Java
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void greetingShouldReturnDefaultMessage() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
                String.class)).contains("Hello World");
    }
}
```

Spring Boot cung cấp sẵn TestRestTemplate và bạn có thể @Autowired để sử dụng nó và gửi HTTP request.

Nếu bạn chỉ muốn test các Controller (Spring handles incomming HTTP request and hands it off to your controller), thì không nhất thiết phải start cả server lên. Spring **MockMvc** sẽ giúp chúng ta test Controller giống như xử lý HTTP request thực sự mà không tốt chi phí bật server.

**Controller Testing**

Để sử dụng MockMvc bạn cần annotation `@AutoConfigureMockMvc` để inject nó và có thể `@Autowired` để dùng.

```Java
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello World")));
    }
}
```

Ví dụ trên, toàn bộ Spring application context được load nhưng không start server. Bạn có thể chỉ cần khởi tạo Web layer thay vì toàn bộ application context bằng cách sử dụng `@WebMvcTest` 

```Java
@WebMvcTest
public class WebLayerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello World")));
    }
}
```

Nếu có nhiều Controller và chỉ cần khởi tạo một controller thì sử dụng `@WebMvcTest(HomeController.class)`.

Ví dụ test Controller có sử dụng Service: 

```Java
@WebMvcTest(GreetingController.class)
public class WebMockTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GreetingService service;

    @Test
    public void greetingShouldReturnMessageFromService() throws Exception {
        when(service.greet()).thenReturn("Hello Mock");
        this.mockMvc.perform(get("/greeting")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello Mock")));
    }
}
```

Trong ví dụ trên, bạn sử dụng `@MockBean` để tạo và inject a mock (fake) cho GreetingService (nếu không thì application context sẽ không start), và cài đặt mong muốn cho service (fake chức năng) bằng cách sử dụng Mockio, cụ thể là dùng static method **when**. Có thể dịch nghĩa ví dụ trên như sau: khi gọi `service.greeting()` thì trả về "Hello Mock".

## References 
https://spring.io/guides/gs/testing-web/
